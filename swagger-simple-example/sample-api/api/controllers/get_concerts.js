'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

var dummyjson = require('dummy-json');
var jstoxml = require('jstoxml');
var o2x = require('object-to-xml');



var concertSchema = '{\
  "id" : "{{@guid}}",\
  "currencyCode" : "{{currencyCode}}",\
  "price" : {{int 0 4999}},\
  "description" : "{{lorem 5}}",\
  "name" : "{{brand}} Design item made by {{firstName}} {{lastName}} Agency from {{city}}",\
  "image" : "img{{@guid}}.png"\
}';

var concertsSchema = '[\
    {{#repeat 10}}\
    '+ concertSchema + '\
    {{/repeat}}\
  ]';
/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */

/**
 * Gets a certain concert
 * Returns a single concert for its ID
 * 
 * concertId String The concert's id
 * currency String Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217http://en.wikipedia.org/wiki/ISO_4217) as currency code
 * returns Concert
**/
module.exports.getConcert = function getConcert(req, res, next) {
  var currency = req.query.currency || 'EUR';
  var concert = req.param("concertId");

  var concerts = {};
  concerts['application/json'] =
  JSON.parse(dummyjson.parse(concertSchema, {mockdata: {"currencyCode": currency}}));

  if (Object.keys(concerts).length > 0) {

    res.setHeader('Content-Type', 'application/json');
    res.json(concerts[Object.keys(concerts)[0]]);
    //res.end(JSON.stringify(concerts[Object.keys(concerts)[0]] || {}, null, 2));
    //res.setHeader('Content-Type', 'application/xml');
    //var result = o2x({"concert" : concerts[Object.keys(concerts)[0]]});
    //console.log(result);
    //res.end(result);
    //req.end(JSON.stringify(concerts[Object.keys(concerts)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

/**
* Provide list of concerts, filtered out by brand or nationwide. The list supports paging.
*
* brand String The name of the brand to filter out concerts (optional)
* pageSize BiggDecimal Number of concerts returned (optional)
* pageNumber BigDecimal Page number (optional)
* currency String Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217http://en.wikipedia.org/wiki/ISO_4217) as currency code
* returns List
**/
module.exports.getConcerts = function getConcerts(req, res, next) {

  var brand = req.query.brand || 'lacoste';
  var pagaSize = req.query.pagaSize || 10;
  var pageNumber = req.query.pageNumber || 1;
  var currency = req.query.currency || 'EUR';



  var concerts = {};
  concerts['application/json'] = /*[{
    "id" : "aeiou",
    "currency" : "aeiou",
    "price" : 0.8008282,
    "description" : "aeiou",
    "name" : "aeiou",
    "image" : "aeiou"
  } ];*/
  JSON.parse(dummyjson.parse(concertsSchema, {mockdata: {"brand" : brand, "correncyCode": currency}}));

  if (Object.keys(concerts).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.json(concerts[Object.keys(concerts)[0]]);
    //res.end(JSON.stringify(concerts[Object.keys(concerts)[0]] || {}, null, 2));
    } else {
      res.end();
    }
};
