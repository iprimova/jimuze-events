const express = require('express');
const router = express.Router();

var faker = require('faker/locale/en_GB.js');
/* GET users listing. */
router.get('/events/reserve', (req, res) => {
  res.send([
  	{
  "events-reservation":
[
	{	"image": faker.image.image(),
		"title": faker.lorem.slug(),
		"date": faker.date.recent(),
		"place": faker.address.city(),
		"weekday": faker.date.weekday()
	},

	{	"image": faker.image.image(),
		"title": faker.lorem.slug(),
		"date": faker.date.recent(),
		"place": faker.address.city(),
		"weekday": faker.date.weekday()
	},

	{	"image": faker.image.image(),
		"title": faker.lorem.slug(),
		"date": faker.date.recent(),
		"place": faker.address.city(),
		"weekday": faker.date.weekday()
	},

	{	"image": faker.image.image(),
		"title": faker.lorem.slug(),
		"date": faker.date.recent(),
		"place": faker.address.city(),
		"weekday": faker.date.weekday()
	},

	{	"image": faker.image.image(),
		"title": faker.lorem.slug(),
		"date": faker.date.recent(),
		"place": faker.address.city(),
		"weekday": faker.date.weekday()
	},
]
}
    ]);
});


router.get('/events/took-place', (req, res) => {
  res.send([
  	{
  "events-took-place":
[
	{
		"title": faker.lorem.slug(),
		"place": faker.address.city(),
		"date": faker.date.weekday(),
		"description": faker.lorem.sentences(),
		"likes": faker.random.number(),
		"comment": faker.lorem.lines(),
	},

	{
		"title": faker.lorem.slug(),
		"place": faker.address.city(),
		"date": faker.date.weekday(),
		"description": faker.lorem.sentences(),
		"likes": faker.random.number(),
		"comment": faker.lorem.lines(),
	},

	{
		"title": faker.lorem.slug(),
		"place": faker.address.city(),
		"date": faker.date.weekday(),
		"description": faker.lorem.sentences(),
		"likes": faker.random.number(),
		"comment": faker.lorem.lines(),
	},

	{
		"title": faker.lorem.slug(),
		"place": faker.address.city(),
		"date": faker.date.weekday(),
		"description": faker.lorem.sentences(),
		"likes": faker.random.number(),
		"comment": faker.lorem.lines(),
	},

	{
		"title": faker.lorem.slug(),
		"place": faker.address.city(),
		"date": faker.date.weekday(),
		"description": faker.lorem.sentences(),
		"likes": faker.random.number(),
		"comment": faker.lorem.lines(),
	},
]
}
    ]);
});

module.exports = router;
