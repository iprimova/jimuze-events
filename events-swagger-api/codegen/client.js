/// <reference path="../../tyings/tsd.d.ts" />

import * as request from "superagent";
import {
    SuperAgentStatic
} from "superagent";

type CallbackHandler = (err: any, res ? : request.Response) => void;
type Event = {
    'id' ? : string

    'name' ? : string

    'description' ? : string

    'likes' ? : number

    'image' ? : string

    'comments' ? : string

};
type Error = {
    'message': string

};

type Logger = {
    log: (line: string) => any
};

/**
 * 
 * @class ConcertService
 * @param {(string)} [domainOrOptions] - The project domain.
 */
export default class ConcertService {

    private domain: string = "http://localhost:10010/api/v1";
    private errorHandlers: CallbackHandler[] = [];

    constructor(domain ? : string, private logger ? : Logger) {
        if (domain) {
            this.domain = domain;
        }
    }

    getDomain() {
        return this.domain;
    }

    addErrorHandler(handler: CallbackHandler) {
        this.errorHandlers.push(handler);
    }

    private request(method: string, url: string, body: any, headers: any, queryParameters: any, form: any, reject: CallbackHandler, resolve: CallbackHandler) {
        if (this.logger) {
            this.logger.log(`Call ${method} ${url}`);
        }

        let req = (request as SuperAgentStatic)(method, url).query(queryParameters);

        Object.keys(headers).forEach(key => {
            req.set(key, headers[key]);
        });

        if (body) {
            req.send(body);
        }

        if (typeof(body) === 'object' && !(body.constructor.name === 'Buffer')) {
            req.set('Content-Type', 'application/json');
        }

        if (Object.keys(form).length > 0) {
            req.type('form');
            req.send(form);
        }

        req.end((error, response) => {
            if (error || !response.ok) {
                reject(error);
                this.errorHandlers.forEach(handler => handler(error));
            } else {
                resolve(response);
            }
        });
    }

    getEventsURL(parameters: {
        'theme' ? : string,
        'place' ? : string,
        'date' ? : string,
        'likes' ? : number,
        'comments' ? : string,
        'currency' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/events';
        if (parameters['theme'] !== undefined) {
            queryParameters['theme'] = parameters['theme'];
        }

        if (parameters['place'] !== undefined) {
            queryParameters['place'] = parameters['place'];
        }

        if (parameters['date'] !== undefined) {
            queryParameters['date'] = parameters['date'];
        }

        if (parameters['likes'] !== undefined) {
            queryParameters['likes'] = parameters['likes'];
        }

        if (parameters['comments'] !== undefined) {
            queryParameters['comments'] = parameters['comments'];
        }

        if (parameters['currency'] !== undefined) {
            queryParameters['currency'] = parameters['currency'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Provide list of events, filtered out by theme or nationwide. The list supports paging. 
     * @method
     * @name ConcertService#getEvents
     * @param {string} theme - The name of the event to filtered out events
     * @param {string} place - Place of events
     * @param {string} date - date of ot the events
     * @param {number} likes - Count of likes
     * @param {string} comments - comments
     * @param {string} currency - Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217) as currency code
     */
    getEvents(parameters: {
        'theme' ? : string,
        'place' ? : string,
        'date' ? : string,
        'likes' ? : number,
        'comments' ? : string,
        'currency' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/events';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = 'application/json';
            headers['Content-Type'] = 'application/json';

            if (parameters['theme'] !== undefined) {
                queryParameters['theme'] = parameters['theme'];
            }

            if (parameters['place'] !== undefined) {
                queryParameters['place'] = parameters['place'];
            }

            if (parameters['date'] !== undefined) {
                queryParameters['date'] = parameters['date'];
            }

            if (parameters['likes'] !== undefined) {
                queryParameters['likes'] = parameters['likes'];
            }

            if (parameters['comments'] !== undefined) {
                queryParameters['comments'] = parameters['comments'];
            }

            if (parameters['currency'] !== undefined) {
                queryParameters['currency'] = parameters['currency'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

    getEventURL(parameters: {
        'eventId': string,
        'currency' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): string {
        let queryParameters: any = {};
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/events/{eventId}';

        path = path.replace('{eventId}', `${parameters['eventId']}`);
        if (parameters['currency'] !== undefined) {
            queryParameters['currency'] = parameters['currency'];
        }

        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                queryParameters[parameterName] = parameters.$queryParameters[parameterName];
            });
        }

        let keys = Object.keys(queryParameters);
        return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '');
    }

    /**
     * Returns a single event for its ID
     * @method
     * @name ConcertService#getEvent
     * @param {string} eventId - The event's id
     * @param {string} currency - Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217) as currency code
     */
    getEvent(parameters: {
        'eventId': string,
        'currency' ? : string,
        $queryParameters ? : any,
        $domain ? : string
    }): Promise < request.Response > {
        const domain = parameters.$domain ? parameters.$domain : this.domain;
        let path = '/events/{eventId}';
        let body: any;
        let queryParameters: any = {};
        let headers: any = {};
        let form: any = {};
        return new Promise((resolve, reject) => {
            headers['Accept'] = 'application/json';
            headers['Content-Type'] = 'application/json';

            path = path.replace('{eventId}', `${parameters['eventId']}`);

            if (parameters['eventId'] === undefined) {
                reject(new Error('Missing required  parameter: eventId'));
                return;
            }

            if (parameters['currency'] !== undefined) {
                queryParameters['currency'] = parameters['currency'];
            }

            if (parameters.$queryParameters) {
                Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
                    queryParameters[parameterName] = parameters.$queryParameters[parameterName];
                });
            }

            this.request('GET', domain + path, body, headers, queryParameters, form, reject, resolve);
        });
    }

}
